package com.example.relativelayout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class StartersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starters);

        ListView startersList = findViewById(R.id.list_view_starters);

        Dish[] dishes = {
                new Dish("Pesto and mushroom wontons","Thin wonton cases stuffed with sun-dried tomato pesto and portobello mushroom",59),
                new Dish("Cheese and crab dumplings","Bakpao kepiting keju",64),
                new Dish("Apple and tuna soup","Apel dikasih sup tuna",45),
                new Dish("Veal and pigeon dumplings","Bakpao unggas",50),
                new Dish("Tomato and chickpea soup","Sup tomat ayam",54),
        };

        ArrayAdapter<Dish> dishAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,dishes);
        startersList.setAdapter(dishAdapter);

        startersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Dish ds = dishAdapter.getItem(i);
                Toast toast = Toast.makeText(getApplicationContext(),ds.getDesc(),Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }
}