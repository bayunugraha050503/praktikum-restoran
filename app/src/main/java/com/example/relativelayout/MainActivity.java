package com.example.relativelayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    CardView starters;
    CardView mains;
    CardView dessert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        starters = findViewById(R.id.card_view_starters);
        mains = findViewById(R.id.card_view_mains);
        dessert = findViewById(R.id.card_view_dessert);

        starters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startersActivityIntent = new Intent(MainActivity.this, StartersActivity.class);
                startActivity(startersActivityIntent);
            }
        });

        mains.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainsActivityIntent = new Intent(MainActivity.this, UtamaActivity.class);
                startActivity(mainsActivityIntent);
            }
        });

        dessert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dessertActivityIntent = new Intent(MainActivity.this, DessertActivity.class);
                startActivity(dessertActivityIntent);
            }
        });

        TextView locText = findViewById(R.id.text_view_loc);
        locText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent launchMapAppIntent = new Intent(Intent.ACTION_VIEW);
                launchMapAppIntent.setData(Uri.parse("geo:-7.273946852840151, 112.79371116759155?q=Politeknik+Elektronika+Negeri+Surabaya"));
                startActivity(launchMapAppIntent);
            }
        });

        TextView orderText = findViewById(R.id.text_view_order);
        orderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent launchEmailAppIntent = new Intent(Intent.ACTION_SENDTO);
                launchEmailAppIntent.setData(Uri.parse("mailto:bayunugraha050503@gmail.com"));
                startActivity(launchEmailAppIntent);
            }
        });

    }
}